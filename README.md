===============================

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/nanoiron/nanoiron.cc,
  appl/nanoiron/nanoiron.input,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/nanoiron/nanoiron.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.#


# Installation

You can build the module just like any other DUNE module. 
For building and running the executables, go to the folders containing
the sources listed above. For the basic dependencies see dune-project.org.

The easiest way is to use the installKissinger2016b.sh in this folder here.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Bilal2016a/installBilal2016a.sh). Create a new folder containing the script and execute it.

You might want to proceed as below in a terminal:
- `mkdir Bilal2016a`
- `cd Bilal2016a`
- `git clone -b master https://git.iws.uni-stuttgart.de/dumux-pub/Bilal2016.git Bilal2016a`
- `cp Bilal2016a/installBilal2016.sh Bilal2016a/Bilal2016.opts .`
- `chmod u+x installBilal2016.sh`
- `./installBilal2016.sh`



