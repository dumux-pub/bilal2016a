// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Quantities required by the single-phase, two-component box
 *        model defined on a vertex.
 */
#ifndef DUMUX_1P2C_VOLUME_VARIABLES_HH
#define DUMUX_1P2C_VOLUME_VARIABLES_HH

#include <dumux/implicit/volumevariables.hh>

#include "properties.hh"

namespace Dumux
{

/*!
 * \ingroup OnePTwoCModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are constant within a
 *        finite volume in the single-phase, two-component model.
 */
template <class TypeTag>
class OnePTwoCVolumeVariables : public ImplicitVolumeVariables<TypeTag>
{
    typedef ImplicitVolumeVariables<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        phaseIdx = Indices::phaseIdx,
        sPhaseIdx = FluidSystem::sPhaseIdx,
        phaseCompIdx = Indices::phaseCompIdx,
        conti0EqIdx = Indices::conti0EqIdx,
        transportCompIdx = Indices::transportCompIdx,
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = FluidSystem:: numComponents
    };
    //indices of primary variables
    enum{
        pressureIdx = Indices::pressureIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        transportEqIdx = Indices::transportEqIdx,
        precipitateEqIdx = Indices::precipitateEqIdx,
        solidFractionIdx = Indices::solidFractionIdx,
        perecipitateCompIdx = Indices::perecipitateCompIdx,
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar,dim> DimVector;
    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;

public:

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx,
                const bool isOldSol)
    {
        ParentType::update(priVars, problem, element, fvGeometry, scvIdx, isOldSol);
        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, fluidState_);

        //calculate all secondary variables from the primary variables and store results in fluidstate
//         completeFluidState(priVars, problem, element, fvGeometry, scvIdx, fluidState_);

        //porosity eveluation
        initialPorosity_ = problem.spatialParams().porosity(element, fvGeometry, scvIdx);
        minimumPorosity_ = problem.spatialParams().minimumPorosity(element, fvGeometry, scvIdx);
        dispersivity_ = problem.spatialParams().dispersivity(element, fvGeometry, scvIdx);

        //volume fraction
        sumPrecipitates_ = 0.0;
        densityOfMedium_ = problem.spatialParams().densityOfMedium();
        densityOfPartDep_ = problem.spatialParams().densityOfPartDep();
//         check = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.ch);
        precipitateVolumeFraction_ = priVars[solidFractionIdx];
        sumPrecipitates_+= precipitateVolumeFraction_;

        //mass fraction of particles


        massPrecipitates_= this->fluidState_.massFraction(phaseIdx, perecipitateCompIdx);
        if(massPrecipitates_ >0){
            S_=this->fluidState_.massFraction(phaseIdx, perecipitateCompIdx);
            moleFractionS_ += this->fluidState_.moleFraction(phaseIdx, perecipitateCompIdx);
        }

        // Porosity of porous medium
        porosityNew_ = std::max(minimumPorosity_, (initialPorosity_ - (sumPrecipitates_)));

        //Permeability evaluation
        initialPermeability_ = problem.spatialParams().intrinsicPermeability(element, fvGeometry, scvIdx);
        fractionOfArea_  = problem.spatialParams().fractionOfArea();
        sufaceAreaOfPart_   = problem.spatialParams().sufaceOfPart();
        densityOfPart_  = problem.spatialParams().densityOfPart();
        moleFracTracer_  = problem.spatialParams().moleFracTracer();
        Scalar theta = 1;
        //Specific surface area of matrix
        bedArea_ = 1.42e4;

        //find surface area of deposited particle
        specificSurfaceArea_ = bedArea_+(theta*(sufaceAreaOfPart_*((sumPrecipitates_))));

        //Permeability

        permeabilityFactor_ = std::pow((porosityNew_/initialPorosity_),3)*std::pow((bedArea_/specificSurfaceArea_),2);
        permeabilityNew_ = permeabilityFactor_*initialPermeability_;

        // Second instance of a parameter cache.
        // Could be avoided if diffusion coefficients also
        // became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updatePhase(fluidState_, phaseIdx);

        diffCoeff_ = FluidSystem::binaryDiffusionCoefficient(fluidState_,
                                                             paramCache,
                                                             phaseIdx,
                                                             phaseCompIdx,
                                                             transportCompIdx);

        Valgrind::CheckDefined(porosityNew_);
        Valgrind::CheckDefined(dispersivity_);
        Valgrind::CheckDefined(diffCoeff_);

        // energy related quantities not contained in the fluid state
        asImp_().updateEnergy_(priVars, problem, element, fvGeometry, scvIdx, isOldSol);
    }

    /*!
     * \copydoc ImplicitModel::completeFluidState
     */
    static void completeFluidState(const PrimaryVariables& priVars,
                                   const Problem& problem,
                                   const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const int scvIdx,
                                   FluidState& fluidState)
    {
        Scalar t = Implementation::temperature_(priVars, problem, element,
                                                fvGeometry, scvIdx);
        fluidState.setTemperature(t);
        fluidState.setSaturation(phaseIdx, 1.);

        fluidState.setPressure(phaseIdx, priVars[pressureIdx]);

        if(useMoles)
        {
            fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1 - (priVars[massOrMoleFracIdx]));
            fluidState.setMoleFraction(phaseIdx, transportCompIdx, priVars[massOrMoleFracIdx] );

        }
        else
        {
            // setMassFraction() has only to be called 1-numComponents times
            fluidState.setMassFraction(phaseIdx, transportCompIdx, priVars[massOrMoleFracIdx] - priVars[solidFractionIdx]);
            fluidState.setMassFraction(phaseIdx, perecipitateCompIdx, priVars[solidFractionIdx]);
        }

        typename FluidSystem::ParameterCache paramCache;
        paramCache.updatePhase(fluidState, phaseIdx);

        Scalar value;
        value = FluidSystem::density(fluidState, paramCache, phaseIdx);
        fluidState.setDensity(phaseIdx, value);
        value = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
        fluidState.setViscosity(phaseIdx, value);

        // compute and set the enthalpy
        Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
        fluidState.setEnthalpy(phaseIdx, h);
    }

    /*!
     * \brief Return the fluid configuration at the given primary
     *        variables
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \brief Return density \f$\mathrm{[kg/m^3]}\f$ the of the fluid phase.
     */
    Scalar density(int phaseIdx) const
    { if(phaseIdx < 1)
            return fluidState_.density(phaseIdx);
        else if (phaseIdx == 1)
            return 7.80e3;
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }




    /*!
     * \brief Return molar density \f$\mathrm{[mol/m^3]}\f$ the of the fluid phase.
     */
    Scalar molarDensity(int phaseIdx) const
    {   if(phaseIdx <= 1)
            return fluidState_.molarDensity(phaseIdx);
        else if (phaseIdx > 1)
            return FluidSystem::precipitateMolarDensity(sPhaseIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Return mole fraction \f$\mathrm{[mol/mol]}\f$ of a component in the phase.
     *
     * \param compIdx The index of the component
     */
    Scalar moleFraction(int compIdx) const
    { return fluidState_.moleFraction(phaseIdx, (compIdx==0)?phaseCompIdx:transportCompIdx); }

    /*!
     * \brief Return mass fraction \f$\mathrm{[kg/kg]}\f$ of a component in the phase.
     *
     * \param compIdx The index of the component
     */
    Scalar massFraction(int compIdx) const
    { return fluidState_.massFraction(phaseIdx, (compIdx==0)?phaseCompIdx:transportCompIdx); }


    /*!
     *
     * \brief Return concentration \f$\mathrm{[mol/m^3]}\f$  of a component in the phase.
     *
     * \param compIdx The index of the component
     */
    Scalar molarity(int compIdx) const
    { return fluidState_.molarity(phaseIdx, (compIdx==0)?phaseCompIdx:transportCompIdx); }

    /*!
     * \brief Return the effective pressure \f$\mathrm{[Pa]}\f$ of a given phase within
     *        the control volume.
     */
    Scalar pressure() const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \brief Return the binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ in the fluid.
     */
    Scalar diffCoeff() const
    { return diffCoeff_; }

    /*!
     * \brief Returns the factor for the reduction of the initial permeability
     * due precipitates in the porous medium
     */
    Scalar permeabilityFactor() const
    { return permeabilityFactor_; }

    /*!
     * \brief Returns the factor for the reduction of the initial permeability
     * due precipitates in the porous medium
     */
    Scalar permeabilityNew() const
    { return permeabilityNew_; }




    /*!
     * \brief Returns the dispersivity of the fluid's streamlines.
     */
    const GlobalPosition &dispersivity() const
    { return dispersivity_; }

    Scalar precipitateVolumeFraction() const
    { return precipitateVolumeFraction_; }



    /*!
     * \brief Return temperature \f$\mathrm{[K]}\f$ inside the sub-control volume.
     *
     * Note that we assume thermodynamic equilibrium, i.e. the
     * temperature of the rock matrix and of all fluid phases are
     * identical.
     */
    Scalar temperature() const
    { return fluidState_.temperature(phaseIdx); }

    /*!
     * \brief Return the dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of a given phase
     *        within the control volume.
     */
    Scalar viscosity() const
    { return fluidState_.viscosity(phaseIdx); }

    /*!
     * \brief Returns the inital porosity of the
     * pure, precipitate-free porous medium
     */
    Scalar initialPorosity() const
    { return initialPorosity_; }

    /*!
     * \brief Returns new porosity of the
     * pure, precipitate-free porous medium
     */
    Scalar porosity() const
    { return porosityNew_; }

    double densityOfMedium() const
    { return densityOfMedium_; }

    /*!
     * \brief Returns the sum of precipitates of the
     * pure, precipitate-free porous medium
     */
    Scalar sumPrecipitates() const
    { return sumPrecipitates_; }

    Scalar massFractionPrec() const
    { return precipitateVolumeFraction_; }

    Scalar moleFractionPrec() const
    { return precipitateVolumeFraction_; }

    Scalar specificVolume() const
    { return (1/densityOfPartDep_); }



    /*!
     * \brief Returns the inital porosity of the
     * pure, precipitate-free porous medium
     */
    Scalar initialPermeability() const
    { return initialPermeability_; }

protected:
    static Scalar temperature_(const PrimaryVariables &priVars,
                               const Problem& problem,
                               const Element &element,
                               const FVElementGeometry &fvGeometry,
                               const int scvIdx)
    {
        return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global);
    }

    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            const int phaseIdx)
    {
        return 0;
    }

    /*!
     * \brief Called by update() to compute the energy related quantities.
     */
    void updateEnergy_(const PrimaryVariables &priVars,
                       const Problem &problem,
                       const Element &element,
                       const FVElementGeometry &fvGeometry,
                       const int scvIdx,
                       const bool isOldSol)
    { }


    GlobalPosition dispersivity_;
    Scalar diffCoeff_;
    Scalar specificSurfaceArea_;
    Scalar cleanBedSurfaceArea_;
    FluidState fluidState_;
    Scalar surfaceArea_;
    Scalar initialPorosity_;
    Scalar porosityNew_;
    Scalar sumPrecipitates_;
    Scalar check;
    Scalar densityOfPartDep_;
    Scalar precipitateVolumeFraction_;
    Scalar massPrecipitates_;
    Scalar densityOfMedium_;
    Scalar minimumPorosity_;
    Scalar initialPermeability_;
    Scalar fractionOfArea_;
    Scalar sufaceAreaOfPart_;
    Scalar densityOfPart_;
    Scalar bedArea_;
    Scalar permeabilityFactor_;
    Scalar moleFractionS_;
    Scalar porosity_;
    Scalar S_;
    Scalar permeabilityNew_;
    Scalar moleFracTracer_;
    Scalar densityParticleDeposit_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};

}// end namespace

#endif
