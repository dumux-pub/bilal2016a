// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup Components
 *
 * \brief Properties of pure molecular oxygen \f$O_2\f$.
 */
#ifndef DUMUX_nZVI_HH
#define DUMUX_nZVI_HH
#include "component.hh"

#include <cmath>

namespace Dumux
{

/*!
 * \ingroup Components
 *
 * \brief Properties of pure molecular oxygen \f$O_2\f$.
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class nZVI : public Component<Scalar, nZVI<Scalar> >
{

public:
    /*!
     * \brief A human readable name for the \f$O_2\f$.
     */
    static const char *name()
    { return "nZVI"; }

    /*!
     * \brief The molar mass \f$\mathrm{[kg/mol]}\f$ of molecular nZVI.
     */
    static Scalar molarMass()
    { return 96e-3; }


    static Scalar density()
    {
        return 6.8e3;
    }

    static Scalar specificSurfaceArea()
    {
        return 705000;
    }

    static Scalar precipitateMolarDensity()
    {
        return density()/molarMass();
    }

};

} // end namespace

#endif
