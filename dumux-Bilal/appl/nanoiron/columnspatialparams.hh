// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_COLUMN_SPATIAL_PARAMS_HH
#define DUMUX_COLUMN_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

/*!
 * \ingroup OnePTwoCModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
class ColumnSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;


    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum {
        // world dimension
        dimWorld = GridView::dimensionworld
    };
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;


    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:
    ColumnSpatialParams(const GridView &gridView)
        : ParentType(gridView)
    {
        permeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
        porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
        longDispersion_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, LongDispersion);
        transDispersion_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, TransDispersion);
        densityOfMedium_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, DensityOfMedium);
        densityOfPartDep_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, DensityOfPartDep);
        fractionOfArea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FractionOfArea);
        sufaceOfPart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SufaceOfPart);
        densityOfPart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, DensityOfPart);
        minimumPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams,MinimumPorosity) ;
        bedArea_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams,BedArea) ;
        moleFracTracer_  = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MoleFracTracer);
        
    }

    ~ColumnSpatialParams()
    {}


    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
    void update(const SolutionVector &globalSolution)
    {
    }

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const Scalar intrinsicPermeability(const Element &element,
                                       const FVElementGeometry &fvGeometry,
                                       const int scvIdx) const
    {
            return permeability_;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    double porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
            return porosity_;
    }
    
    /*!
     * \brief Define the minimum porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    double minimumPorosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
            return minimumPorosity_;
    }
    
    /*!
     * \brief Returns the bulk density of porous medium  porosity of the
     * 
     */      
    
    double densityOfMedium() const
    {
            return densityOfMedium_;
    }
    
    /*!
     * \brief Returns the density of nZVI particles of the
     * 
     */
    
    double densityOfPartDep() const
    {
            return densityOfPartDep_;
    }
    
    /*!
     * \brief Returns the fraction area of 
     * the deposited particles
     */
    double fractionOfArea() const
    {
            return fractionOfArea_ ;
    }
    
    
    /*!
     * \brief Returns the clean Bed surface area of the
     * pure, precipitate-free porous medium
     */
    double bedArea() const
    {
            return bedArea_ ;
    }
    
    /*!
     * \brief Returns the inital porosity of the
     * pure, precipitate-free porous medium
     */
    
    double sufaceOfPart() const
    {
            return sufaceOfPart_  ;
    }
    
    /*!
     * \brief Returns the density of 
     * nZVI particles
     */
    double densityOfPart() const
    {
            return densityOfPart_ ;
    }
    
    /*!
     * \brief Returns the concentration of 
     * tracer
     */
    double moleFracTracer() const
    {
        return moleFracTracer_;
    }
    
    
    
    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    GlobalPosition dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {

        GlobalPosition dispersivity;
        dispersivity[0]= longDispersion_;
        dispersivity[1]= transDispersion_;
        dispersivity[2]= transDispersion_;
        return dispersivity;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar heatCapacity(const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvGeometry, scvIdx));
//    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar thermalConductivitySolid(const Element &element,
//                                    const FVElementGeometry &fvGeometry,
//                                    const int scvIdx) const
//    {
//        return lambdaSolid_;
//    }



private:
    //Input parameters declaration
    Scalar permeability_;
    Scalar porosity_;
    Scalar longDispersion_;
    Scalar transDispersion_;
    Scalar densityOfPartDep_;
    Scalar densityOfMedium_; 
    Scalar fractionOfArea_;
    Scalar surfaceOfPart_;
    Scalar densityOfPart_;
    Scalar sufaceOfPart_;
    Scalar minimumPorosity_;
    Scalar bedArea_;
    Scalar moleFracTracer_;
   
    
//    Scalar lambdaSolid_;
};

}

#endif
