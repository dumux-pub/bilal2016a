// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Definition of a problem, for the column problem:
 * Component transport of nano iron particles in the water phase.
 */
#ifndef DUMUX_COLUMN_PROBLEM_HH
#define DUMUX_COLUMN_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <math.h>

#include <dumux/porousmediumflow/1p2cmin/implicit/model.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/io/cubegridcreator.hh>
#include <dumux/material/components/h2o.hh>

// #include "localresidual.hh"
#include "nanoironfluidsystem.hh"
#include "columnspatialparams.hh"
// #include "volumevariables.hh"



#define NONISOTHERMAL 0

namespace Dumux
{

template <class TypeTag>
class ColumnProblem;

namespace Properties
{
#if NONISOTHERMAL
NEW_TYPE_TAG(ColumnProblem, INHERITS_FROM(OnePTwoCNI));
NEW_TYPE_TAG(ColumnBoxProblem, INHERITS_FROM(BoxModel, ColumnProblem));
NEW_TYPE_TAG(ColumnCCProblem, INHERITS_FROM(CCModel, ColumnProblem));
#else
NEW_TYPE_TAG(ColumnProblem, INHERITS_FROM(OnePTwoC));
NEW_TYPE_TAG(ColumnBoxProblem, INHERITS_FROM(BoxModel, ColumnProblem));
NEW_TYPE_TAG(ColumnCCProblem, INHERITS_FROM(CCModel, ColumnProblem));

#endif

// Set the grid type
// #if HAVE_UG
// SET_TYPE_PROP(ColumnProblem, Grid, Dune::UGGrid<3>);
// #else
// SET_TYPE_PROP(ColumnProblem, Grid, Dune::YaspGrid<3>);
// #endif

// Set the grid type
#if HAVE_DUNE_ALUGRID
SET_TYPE_PROP(ColumnProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);
#else
SET_TYPE_PROP(ColumnProblem, Grid, Dune::YaspGrid<3>);
#endif

// Set the wetting phase
//SET_PROP(ColumnProblem, WettingPhase)
//{
//private: typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//public: typedef Dumux :: FluidSystems :: LiquidPhase < Scalar , Dumux :: H2O < Scalar > > type ;
//
//};
////Use the cube grid creator
//SET_TYPE_PROP(ColumnProblem, GridCreator, Dumux::CubeGridCreator<TypeTag>);
// Set the problem property
SET_TYPE_PROP(ColumnProblem, Problem, Dumux::ColumnProblem<TypeTag>);
// Set the local residual
//SET_TYPE_PROP(ColumnProblem, LocalResidual, Dumux::RetardOnePTwoCLocalResidual<TypeTag>);

// Set fluid configuration
SET_TYPE_PROP(ColumnProblem,
              FluidSystem,
              Dumux::FluidSystems::NanoIronFluidSystem<TypeTag>);

// // Set volume variables
// SET_TYPE_PROP(ColumnProblem,
//               VolumeVariables,
//               Dumux::OnePTwoCVolumeVariables<TypeTag>);

// SET_TYPE_PROP(ColumnProblem,
//               LocalResidual,
//               Dumux::OnePTwoCLocalResidual<TypeTag>);


// Set the spatial parameters
SET_TYPE_PROP(ColumnProblem,
              SpatialParams,
              Dumux::ColumnSpatialParams<TypeTag>);


// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(ColumnProblem, UseMoles, true);

// Disable gravity
SET_BOOL_PROP(ColumnProblem, ProblemEnableGravity, false);

// SET_TYPE_PROP(ColumnProblem, PointSource, Dumux::TimeDependentPointSource<TypeTag>);
}


/*!
 * \ingroup OnePTwoCModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of a problem, for the 1p2c problem:
 * Nitrogen is dissolved in the water phase and
 * is transported with the water flow from the left side to the right.
 *
 * The model domain is 1 m times 1 m with a discretization length of 0.05 m
 * and homogeneous soil properties (\f$ \mathrm{K=10e-10, \Phi=0.4, \tau=0.28}\f$).
 * Initially the domain is filled with pure water.
 *
 * At the left side, a Dirichlet condition defines a nitrogen mole fraction
 * of 0.3 mol/mol.
 * The water phase flows from the left side to the right due to the applied pressure
 * gradient of 1e5 Pa/m. The nitrogen is transported with the water flow
 * and leaves the domain at the right boundary
 * where an outflow boundary condition is applied.
 *
 * The model is able to use either mole or mass fractions. The property useMoles can be set to either true or false in the
 * problem file. Make sure that the according units are used in the problem setup. The default setting for useMoles is true.
 *
 * This problem uses the \ref OnePTwoCModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p2c -parameterFile ./test_box1p2c.input</tt> or
 * <tt>./test_cc1p2c -parameterFile ./test_cc1p2c.input</tt>
 */
template <class TypeTag>
class ColumnProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, PointSource) PointSource;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld

    };
    enum {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        solidFractionIdx = Indices::solidFractionIdx,
        sPhaseIdx = FluidSystem::sPhaseIdx,
        wPhaseIdx = FluidSystem::wPhaseIdx,

#if NONISOTHERMAL
        temperatureIdx = Indices::temperatureIdx
#endif
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    enum {
        // index of the transport equation
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::transportEqIdx,
        precipitateEqIdx = Indices::precipitateEqIdx,

#if NONISOTHERMAL
        energyEqIdx = Indices::energyEqIdx
#endif
    };


    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

//    typedef Dune::UGGrid<dim>::mark(int refCount, const typename Traits::template Codim< 0 >::Entity & 	e) mark;

    //! property that defines whether mole or mass fractions are used
        static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    ColumnProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , eps_(1e-6)
    {
        //initialize fluid system
        FluidSystem::init();

        try
        {
        std::cout<<"Start reading input file"<<std::endl;
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        pointInjectionRate_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PointInjectionRate);
        dirichletPressure_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DirichletPressure);
        multiplier_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.Multiplier);
        exponent_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.Exponent);
        depositionRate_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DepositionRate);
        releaseRate_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.ReleaseRate);
        initialConc_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitCon);
        densityW_ = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.ReferenceDensity);
        episodeLength_ = GET_RUNTIME_PARAM(TypeTag, Scalar, TimeManager.EpisodeLength);
        episodeIndex_= GET_RUNTIME_PARAM(TypeTag, Scalar, TimeManager.EpisodeIndex);
        moleFracTracer_= GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MoleFracTracer);
        

        this->timeManager().startNextEpisode(episodeLength_);
        this->timeManager().setTimeStepSize(10);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the problem file!\n";
            exit(1);
        }


        //stating in the console whether mole or mass fractions are used
        if(useMoles)
        {
            std::cout<<"problem uses mole fractions"<<std::endl;
        }
        else
        {
            std::cout<<"problem uses mass fractions"<<std::endl;
        }
        injectionVolume_ = 0.0;
        int counter = 0;


        {
            FVElementGeometry fvGeometry;
            ElementIterator eIt = this->gridView().template begin<0>();
            ElementIterator elemEndIt = this->gridView().template end<0>();
            //Find the injection volume
            for (; eIt != elemEndIt; ++eIt)
            {
                fvGeometry.update(this->gridView(), *eIt);
                int numVerts = eIt->template count<dim> ();
                for (int scvIdx = 0; scvIdx < numVerts; ++scvIdx)
                {
                    const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
                    if(onInjectionBoundary_(globalPos))
                    {
                        injectionVolume_ += fvGeometry.subContVol[scvIdx].volume;
                        counter += 1;
                    }
                }
            }
        }

        //Moles of water
        molarInjectionRate_ = pointInjectionRate_*1003.4/FluidSystem::molarMass(pressureIdx); // [mol_water/s]
        //Moles of tracer
        molarInjectionTracer_ = pointInjectionRate_*moleFracTracer_/FluidSystem::molarMass(massOrMoleFracIdx);  // [mol_nZVI/s]
        

        //Print values of variables
        std::cout<<"Injection Rate is "<< molarInjectionRate_ << " mol_water/sec" << std::endl;
        std::cout<<"Injection Volume: "<<injectionVolume_<<std::endl;

    }


    bool shouldWriteOutput() const
       {

           return
           this->timeManager().timeStepIndex() == 0 || (this->timeManager().timeStepIndex() > 0) ;
       }




//
   void episodeEnd()
      {
          // Start new episode if episode is over and assign new boundary conditions
          this->timeManager().startNextEpisode(episodeLength_);
          std::cout << "Episode index is set to: " << this->timeManager().episodeIndex() << std::endl;
          this->timeManager().setTimeStepSize(50);
      }


    void addOutputVtkFields()
                  {
                  typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

                   // get the number of degrees of freedom
                   unsigned numDofs = this->model().numDofs();
                   unsigned numElements = this->gridView().size(0);

                   //create required scalar fields
                   ScalarField *Kxx = this->resultWriter().allocateManagedBuffer(numDofs);
                   ScalarField *concentration_tracer = this->resultWriter().allocateManagedBuffer(numDofs);
//                    ScalarField *tracer = this->resultWriter().allocateManagedBuffer(numDofs);

      //             (*boxVolume) = 0;

                   //Fill the scalar fields with values
                   ScalarField *rank = this->resultWriter().allocateManagedBuffer(numElements);

                   FVElementGeometry fvGeometry;
                   VolumeVariables volVars;

                   ElementIterator elemIt = this->gridView().template begin<0>();
                   ElementIterator elemEndIt = this->gridView().template end<0>();
                   for (; elemIt != elemEndIt; ++elemIt)
                   {
//                        int eIdx = this->elementMapper().index(elemIt)
                       int idx = this->elementMapper().index(*elemIt);
                       (*rank)[idx] = this->gridView().comm().rank();
                       fvGeometry.update(this->gridView(), *elemIt);


                       for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                       {
                           int globalIdx = this->model().dofMapper().map(*elemIt, scvIdx, dofCodim);
                           volVars.update(this->model().curSol()[globalIdx],
                                          *this,
                                          *elemIt,
                                          fvGeometry,
                                          scvIdx,
                                          false);
                            
                            Scalar volFraction_=volVars.precipitateVolumeFraction();
                            Scalar ConcnZVI = (densityW_*(volVars.massFraction(massOrMoleFracIdx)))/initialConc_;                                           
                            (*concentration_tracer)[globalIdx] = ConcnZVI; //[normalized concentration]
                           (*Kxx)[globalIdx] = this->spatialParams().intrinsicPermeability(elemIt, fvGeometry, scvIdx)*volVars.permeabilityFactor(); // [modified permeability]
                           
                       }
                   }

                   //pass the scalar fields to the vtkwriter
                    this->resultWriter().attachDofData(*concentration_tracer, "concentration_zVI", true); //element data

                    this->resultWriter().attachDofData(*Kxx, "permeability", true);
                }




    bool shouldWriteRestartFile()
       {
           return false;
       }



    void postTimeStep()
       {
           // Calculate storage terms
           PrimaryVariables storage;
           this->model().globalStorage(storage);


           // Write mass balance information for rank 0
           if (this->gridView().comm().rank() == 0) {
               std::cout<<"Total moles water=[" << storage[pressureIdx] << "]; ";
               std::cout<<"Total moles tracer=[" << storage[massOrMoleFracIdx] << "]; ";

               std::cout<< "Total moles solid=[" << storage[precipitateEqIdx] << "]\n";

           }
       }





    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

#if !NONISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain [K].
     *
     * This problem assumes a temperature of 20 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 20; } // in [K]

#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        if(onConstantHeadBoundary_(globalPos)){

            values.setDirichlet(pressureIdx);
            values.setOutflow(massOrMoleFracIdx);

        }
        else{
        	values.setAllNeumann();
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {

        initial_(values, globalPos);
    }


    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations [kg / (m^2 *s )]
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumannAtPos(PrimaryVariables &values,
                         const GlobalPosition &globalPos) const
       {

           values = 0.0;
       }






    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a priVars parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    //! \copydoc Dumux::ImplicitProblem::solDependentSource()
    void solDependentSource(PrimaryVariables &values,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        const GlobalPosition globalPos = element.geometry().corner(scvIdx);
        const auto& volVars = elemVolVars[scvIdx];
        Scalar DensityOfMedium = this->spatialParams().densityOfMedium();
        Scalar massFraction_=volVars.precipitateVolumeFraction(); //[volume fraction of solid particles]
        Scalar ConcnZVIIdeal_ = densityW_*(volVars.massFraction(massOrMoleFracIdx)); // [concentration of suspended particles]
        Scalar porosity_ = volVars.porosity(); //[modified porosity]
         
        if(this->timeManager().episodeIndex()==episodeIndex_){
            if (onInjectionBoundary_(globalPos))
            {
                    values[conti0EqIdx] = molarInjectionRate_/injectionVolume_;
                    values[transportEqIdx] = molarInjectionTracer_/injectionVolume_;
            }
        }

        else{
            if (onInjectionBoundary_(globalPos))
            {
                    values[conti0EqIdx] = molarInjectionRate_/injectionVolume_;


            }
        }

        // for conservatove tracer set the source term to zero
        values[precipitateEqIdx] = ((porosity_*depositionRate_  *(1+(multiplier_  *std::pow(massFraction_,exponent_  )))*
        (ConcnZVIIdeal_))-(DensityOfMedium*releaseRate_*massFraction_));
    }


    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

//     void addPointSources(std::vector<PointSource>& pointSources) const
//     {
//         // inject 10 kg/s water at position (0, 0);
//         pointSources.push_back(PointSource({0, 0, 0}, {1}));
//     }


private:



    bool onConstantHeadBoundary_(const GlobalPosition &globalPos) const
    {
//        Scalar angle = convertToPolarCoordinates_(globalPos)[1];
        return globalPos[2] < eps_ ;
    }


    bool onBackBoundary_(const GlobalPosition &globalPos) const
        {
    		return globalPos[1] < eps_;

        }



    bool onInjectionBoundary_(const GlobalPosition &globalPos) const
    {

        return globalPos[2] > this->bBoxMax()[2] - eps_ ;
    }




    void initial_(PrimaryVariables &priVars,
                      const GlobalPosition &globalPos) const
        {

  
    		priVars[pressureIdx] = 1e5 ;
    		priVars[massOrMoleFracIdx] =  0;
            priVars[solidFractionIdx] =  0;   // initial condition for the N2 molefraction
        }



    const Scalar eps_;
    Scalar injectionRate_;
    Scalar injectionTracer_;
    Scalar moleFracTracer_;
    Scalar dirichletPressure_;
    std::string name_;
    Scalar alphaMax_;
    Scalar radiusMax_;
    Scalar radiusWell_;
    Scalar densityW_;
    Scalar pointInjectionRate_;
    Scalar molarInjectionRate_;
    Scalar injectionHeight_;
    Scalar injectionVolume_;
    Scalar reductionFactor_;
    Scalar episodeLength_;
    Scalar nZVIPropor_;
    Scalar stablProp_;
    Scalar multiplier_ ;
    Scalar exponent_ ;
    Scalar initialConc_;
    Scalar densityOfMedium_;
    Scalar molarInjectionTracer_;
    Scalar timeStepSize_;
    Scalar molarInjectionStabilizer_;
    Scalar depositionRate_;
    Scalar releaseRate_;
    Scalar episodeIndex_;
    


};

} //end namespace
#endif
