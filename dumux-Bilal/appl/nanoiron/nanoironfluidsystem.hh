// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2011 by Andreas Lauser                                    *
 *   Copyright (C) 2010 by Bernd Flemisch                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A fluid system with one phase and two components
 *        (interstitial fluid and TRAIL, a therapeutic agent for
 *        cancer therapy).
 */
#ifndef DUMUX_NANOIRON_FLUID_SYSTEM_HH
#define DUMUX_NANOIRON_FLUID_SYSTEM_HH

#include <dune/common/exceptions.hh>

#include <dumux/material/fluidsystems/basefluidsystem.hh>
#include <dumux/material/components/h2o.hh>
#include "nZVI.hh"
#include <dumux/material/components/tabulatedcomponent.hh>
#include <assert.h>

#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>


namespace Dumux
{
namespace FluidSystems
{

/*!
 * \ingroup Fluidsystems
 *
 * \brief A fluid system with one phase and two components
 *        (interstitial fluid and TRAIL, a therapeutic agent for
 *        cancer therapy).
 *
 * A fluid system with one phase and two components representing an
 * interstitial fluid that contains therapeutic agent (TRAIL). This is
 * used in conjunction the 1p2c model.
 */
template <class Scalar>
class NanoIron
: public BaseFluidSystem<Scalar, NanoIron<Scalar> >
{

public:
    typedef NanoIron<Scalar> ThisType;
    typedef BaseFluidSystem<Scalar, ThisType> Base;
    typedef Dumux::H2O<Scalar> H2O_IAPWS;
    typedef Dumux::TabulatedComponent<Scalar, H2O_IAPWS> H2O;
    typedef Dumux::nZVI<Scalar> nZVI;



    /****************************************
     * Fluid phase related static parameters
     ****************************************/

//    typedef H2O_IAPWS H2O;
//! Number of phases in the fluid system
    static constexpr int numPhases = 1;

    static const int numSPhases = 1;// precipitated solid phases
    static const int lPhaseIdx = 0; // index of the liquid phase
    static const int gPhaseIdx = 1; // index of the gas phase
    static const int sPhaseIdx = 2; // index of the precipitated salt
    static const int wPhaseIdx = lPhaseIdx; // index of the wetting phase
    static const int nPhaseIdx = gPhaseIdx; // index of the non-wetting phase

    //! Index of the liquid phase


    static const int wCompIdx = wPhaseIdx;
    static const int nCompIdx = nPhaseIdx;


    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static const char *phaseName(int phaseIdx)
    {
//        static const char *name[] = {
//            "l"
//        };
//
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//        return name[phaseIdx];
        switch (phaseIdx) {
        case wPhaseIdx: return "liquid";
        case nPhaseIdx: return "gas";
        case sPhaseIdx: return "nZVI";
        };
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Return whether a phase is liquid
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
            return phaseIdx != nPhaseIdx;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are indepent on the fluid composition. This assumtion is true
     * if Henry's law and Rault's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // we assume Henry's and Rault's laws for the water phase and
        // and no interaction between gas molecules of different
        // components, so all phases are ideal mixtures!
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return true;
    }

    /****************************************
     * Component related static parameters
     ****************************************/

    //! Number of components in the fluid system
    static const int numComponents = 3; // H2O,
//     static const int numMajorComponents = 2;// H2O, Air

    static const int H2OIdx = 0;//0
    static const int IronIdx = 1;//1
//     static const int nZVI = nCompIdx;//2





    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static const char *componentName(int compIdx)
    {
        static const char *name[] = {
            "Water",
            "Iron",
            "Precipitate"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    /*!
     * \brief Return the molar mass of a component in [kg/mol].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            18e-3, // [kg/mol],
            332e-3, // [kg/mol]
            52e-3,
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /*!
     * \brief Return the mass density of the precipitate \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param phaseIdx The index of the precipitated phase to consider
     */
    static Scalar precipitateDensity(int phaseIdx)
    {
        if(phaseIdx <= 1)
            DUNE_THROW(Dune::InvalidStateException, "Invalid solid phase index " << phaseIdx);
        return nZVI::density();
    }


    static Scalar precipitateMolarDensity(int phaseIdx)
     {
         if(phaseIdx == 2)
             return nZVI::precipitateMolarDensity();
         else
             DUNE_THROW(Dune::InvalidStateException, "Invalid solid phase index " << phaseIdx);
     }

    /****************************************
     * thermodynamic relations
     ****************************************/

    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
//        init(/*startTemp=*/273.15, /*endTemp=*/623.15, /*tempSteps=*/100,
//                /*startPressure=*/1e4, /*endPressure=*/40e6, /*pressureSteps=*/200);
    }

    static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
            Scalar startPressure, Scalar endPressure, int pressureSteps)
    {
//        if(H2O::isTabulated)
//        {
//            std::cout << "Initializing tables for the pure-water properties.\n";
//            H2O::init(startTemp, endTemp, tempSteps,
//                    startPressure, endPressure, pressureSteps);
//        }
        // set the salinity of brine
        //            BrineRawComponent::salinity = salinity;
        //
        //            if(Brine::isTabulated)
        //            {
        //                std::cout << "Initializing tables for the brine fluid properties.\n";
        //                Brine::init(startTemp, endTemp, tempSteps,
        //                                      startPressure, endPressure, pressureSteps);
        //            }
    }


    /*!
     * \brief Return the phase density [kg/m^3].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::density;
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//
//        Scalar temperature = fluidState.temperature(phaseIdx);
//        Scalar pressure = fluidState.pressure(phaseIdx);
//        Scalar massFracSalt = fluidState.massFraction(phaseIdx, IronIdx);
//     //   return 1000;
//
//     if (phaseIdx == nPhaseIdx)
//     {
//     return 9999;
//     }
//   else
//       return Brine::liquidDensity(temperature, pressure, massFracSalt);
//
        DUNE_THROW(Dune::NotImplemented, "density in parent type of fluidsystem.");

    }

    /*!
     * \brief Calculate the fugacity coefficient [Pa] of an individual
     *        component in a fluid phase
     *
     * The fugacity coefficient \f$\phi_\kappa\f$ is connected to the
     * fugacity \f$f_\kappa\f$ and the component's molarity
     * \f$x_\kappa\f$ by means of the relation
     *
     * \f[ f_\kappa = \phi_\kappa * x_{\kappa} \f]
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    using Base::fugacityCoefficient;
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        assert(0 <= compIdx  && compIdx < numComponents);
        if (phaseIdx == wPhaseIdx) {
                  if (compIdx == H2OIdx)
                      return 1000;
                  else
                  return 0.5;
              }
        else

        return 1.0;
    }

    /*!
     * \brief Return the dynamic viscosity of a phase [Pa s].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::viscosity;
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//        Scalar temperature = fluidState.temperature(phaseIdx);
//        Scalar pressure = fluidState.pressure(phaseIdx);
//        Scalar massFracSalt = fluidState.massFraction(phaseIdx, IronIdx);
//
//        if (phaseIdx == nPhaseIdx)
//        {
//        return 9999;
//        }
//        else
//          return Brine::liquidViscosity(temperature, pressure, massFracSalt);
        DUNE_THROW(Dune::NotImplemented, "viscosity in parent type of fluidsystem.");
    }

    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase [mol^2 * s / (kg*m^3)]
     *
     * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \mathbf{grad} mu_\kappa \f]
     *
     * where \f$\mu_\kappa\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
     * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
     * fugacity \f$f_\kappa\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
     * pressure and temperature.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    using Base::diffusionCoefficient;
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        // TODO!
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    }


    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient for components
     *        \f$i\f$ and \f$j\f$ in this phase.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     */
    using Base::binaryDiffusionCoefficient;
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)

    {
        DUNE_THROW(Dune::NotImplemented, "binaryDiffusionCoefficient in parent type of fluidsystem.");
    }

    /*!
     * \brief Given a phase's composition, temperature, pressure and
     *        density, calculate its specific enthalpy [J/kg].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx  for which phase to give back the heat capacity
     */
    using Base::enthalpy;
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                                 int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
        Scalar massFracSalt = fluidState.massFraction(phaseIdx, IronIdx);


        if (phaseIdx == nPhaseIdx)
        {
        return 9999;
        }
        else
          return H2O::liquidEnthalpy(temperature, pressure);

//        DUNE_THROW(Dune::NotImplemented, "Enthalpies");
    }

    /*!
     * \brief Thermal conductivity of a fluid phase [W/(m^2 K/m)].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx  for which phase to give back the heat capacity
     */
    using Base::thermalConductivity;
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if (phaseIdx == wPhaseIdx)
            return  0.6; // conductivity of water[W / (m K ) ]

        else
            return 9999;
//            DUNE_THROW(Dune::NotImplemented, "Thermal conductivities.");
    }

    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/kg]}\f$.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::heatCapacity;
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if(phaseIdx == wPhaseIdx)
            return H2O::liquidHeatCapacity(fluidState.temperature(phaseIdx),
                                           fluidState.pressure(phaseIdx));
        else
            return 9999;
//            DUNE_THROW(Dune::NotImplemented, "Heat capacities.");
    }

private:




   };






/*!
 * \brief A pure single-phase fluid system.
 *
 * This is an adapter to use Dumux::NanoIronFluidSystem<TypeTag>, as is
 * done with most other classes in Dumux and all template parameters
 * are usually defined in the property system anyhow.
 */
template<class TypeTag>
class NanoIronFluidSystem: public FluidSystems::NanoIron<typename GET_PROP_TYPE(TypeTag, Scalar)>
{
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename FluidSystems::NanoIron<Scalar> ParentType;
    typedef NanoIronFluidSystem<TypeTag> ThisType;


public:

    typedef Dumux::NullParameterCache ParameterCache;

    //Density calculation according to Diersch and Kolditz, 2002 Eq. 20
    //in [kg/m3]
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,  const ParameterCache &paramCache, int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar gamma = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, Compressibility);
        Scalar referenceDensity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferenceDensity);
        Scalar referencePressure = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferencePressure );
        Scalar density = referenceDensity*(1 + gamma*(fluidState.pressure(ParentType::wPhaseIdx) - referencePressure));
        return density;
    }

    //Viscosity calculation according to Diersch and Kolditz, 2002 Table 5 variable fluid viscosity
    //in [Pa s]
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,  const ParameterCache &paramCache, int phaseIdx)
    {

        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar viscosityNormal = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferenceViscosity);
//         Scalar sPhaseIdx = 1;
//         Scalar molFractt_ = (fluidState.moleFraction(phaseIdx, sPhaseIdx));
//         Scalar molFract_=10000*molFractt_;
// //         std::cout<<"MOLE"<<molFract_<<std::endl;
//         Scalar E_=4.59;
//         Scalar A_ = 5.06e-4;
//         Scalar B_=4.04;
//         Scalar u_1=3.6e-3;
//         Scalar Alpha_ =1;
//         Scalar X_=0.713;
//         Scalar Visc_ =0.0;
//         Scalar Per_=1.45e-10*0.37;
//         Scalar u_2 = viscosityNormal*(1+std::pow(molFract_,E_));
//         Scalar u_3 = u_1-(u_1-viscosityNormal)*(std::pow(1e1,-(molFract_)));
//         Scalar gema_ = 1.9e-3/(std::pow(Per_,0.5));
//         Scalar lambda_= A_*(std::pow((molFract_),B_));
//         Scalar viscosit = u_3 + (u_2-u_3)/(1+(std::pow((lambda_*gema_),X_)));
//         std::cout<<"viscosit"<<viscosit<<std::endl;
        return viscosityNormal;

    }

    //Get the diffusion coefficient from the parameter file
    //in [m2/s]
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState, const ParameterCache &paramCache,
                                                int phaseIdx,
                                                int compIIdx,
                                                int compJIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar binaryDiffusionCoefficient = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, DiffusionCoefficient);
        return binaryDiffusionCoefficient;
    }
};
} // end namepace FluidSystem
} // end namepace Dumux

#endif
