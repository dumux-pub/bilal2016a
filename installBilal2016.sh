

#!/bin/sh
### Create a folder for the dune and dumux modules
### Go into the folder and execute this script
if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi
### DUNE modules
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-localfunctions.git
### dune-alugrid
git clone -b releases/2.4 https://gitlab.dune-project.org/extensions/dune-alugrid.git
### dumux
git clone -b releases/2.9 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
### dunecontrol
./dune-common/bin/dunecontrol --opts=Bilal2016.opts all
