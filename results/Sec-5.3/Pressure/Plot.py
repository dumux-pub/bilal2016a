# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 14:39:01 2016

@author: Bilal
"""
import argparse
import xlrd 
from itertools import izip_longest
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
from decimal import *
import math
import pandas as pd
import matplotlib
import matplotlib.ticker as mtick
from matplotlib import rcParams


def main():
    pass

if __name__ == '__main__':
    main()
    
    path  = (r'../../../')   // Input path
    
    filename = '...xls'
    fn=os.path.join(path, filename)

    # load excel file
    workbook = xlrd.open_workbook(fn)
    sheet = workbook.sheet_by_index(0)  
    
    dataInput = [[str(sheet.cell_value(r,d)) for r in range (sheet.nrows)] for d in range(1)]
    time= np.array(dataInput, dtype=float) 
    time = time.ravel()
    
#        Read input file
            
    dataInput = [[str(sheet.cell_value(r,c)) for r in range (sheet.nrows)] for c in range(2)]
    conc= np.array(dataInput, dtype=float)  
    conc = conc.ravel()        
    
    #plot the data
    plt.ioff()
    plt.figure(figsize=(6,6))
    plt.plot(time,conc)
    plt.rcParams.update({'font.size':30})
##        yticks=np.arange(1.3e-10,1.44e-10+0.05e-10,0.05e-10)
##        plt.yticks(yticks, fontsize = 30)
##    xticks = np.arange(0,6+1.5,1.5)
##    plt.xticks(xticks, fontsize = 30)
#    ax1 = plt.twinx()
#    ax1.plot(time1,c2)
    plt.xlabel('Time [days]', fontsize=30)
    plt.ylabel('Concentration [mg/l]', fontsize=30)
    plt.tight_layout()
    plt.grid(False)
            
    plt.legend(['Simulated nZVI'], loc= 'upper right', prop={'size':25})
    plt.show()
    plt.close()




