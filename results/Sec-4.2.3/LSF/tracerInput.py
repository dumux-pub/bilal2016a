# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 14:39:01 2016

@author: Bilal
"""
import argparse
import xlrd 
from itertools import izip_longest
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
from decimal import *
import math
import pandas as pd
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# parse arguments
parser = argparse.ArgumentParser(description='Extract the data from a csv tracer test file and calculate different transport parameters.')
parser.add_argument('-i', '--inputDirectory', nargs=1, default='.', help="Directory in which the input file is located. Default: '[Current directory]'")
parser.add_argument('-f', '--files', nargs='+', required=True, help="Input file names to be processed. Usage: '[-f inputFile.csv[file2...]]'")
parser.add_argument('-o', '--outputDirectory', nargs=1, default='.', help="Directory to which the data files are written. Default: '[Current directory]'")
parser.add_argument('-l', '--length', type=float, nargs=1, default='0.24', help="Length of column. Default: '[0.24 m]'")
parser.add_argument('-c', '--concentration',type=float, nargs=1, default='1000', help="Concentration of Tracer. Default: '[1000.0 mg/l]'")
parser.add_argument('-vol', '--volume',type=float, nargs=1, default='0.0232', help="Total injected volume of tracer. Default: '[0.0232 l]'")
parser.add_argument('-m', '--mass', type=float,nargs=1, default='23.2', help="Total mass of tracer. Default: '[23.2 mg]'")
parser.add_argument('-Q', '--discharge',type=float, nargs=1, default='0.000805', help="Water injection rate. Default: '[0.000805 l/s]'")
parser.add_argument('-PV', '--vol', type=float,nargs=1, default='0.1306', help="Volume of. Default: '[0.1306 l]'")
parser.add_argument('-v', '--verbosity', type=int, default=2, help='Verbosity of the output. 1 = print progress. 2 = print data columns')
args = vars(parser.parse_args())


# import locations
inDirectory = ''.join(args['inputDirectory'])
inDirectory += '/'
outDirectory = ''.join(args['outputDirectory'])
outDirectory += '/'
if not outDirectory == '':
    if not os.path.exists(outDirectory):
        os.makedirs(outDirectory)

# Declaring variables
confirmation = 0
editEnd=1
counter = 1


    
# loop over all files

for curFile in args['files']:
        # print progress to command line
        fileWithoutPath = os.path.basename(curFile)
        basename = os.path.splitext(fileWithoutPath)[0]
        if args['verbosity'] == 1:
            print("Processing file ({}/{}): {}".format(counter, len(args['files']), inDirectory+curFile))
        counter += 1
    
        # load excel file
        workbook = xlrd.open_workbook(inDirectory+curFile)
        sheet = workbook.sheet_by_index(0)    
        
        #Read input file
        dataInput = [[str(sheet.cell_value(r,c)) for r in range (sheet.nrows)] for c in range(sheet.ncols)]
        c= np.array(dataInput[0], dtype=float)  
        time= np.array(dataInput[1], dtype=float)  
        c = c.ravel()
        time = time.ravel()
               
        #Update PV values according to time
        PV = [(args['discharge']*t/args['vol']) for tvalue, t in enumerate(time)] 
        PV = np.array(PV)        
        
        # cummulative mass
        cummMass = np.zeros((c.shape[0]-3))
        try:
            cummMass = [((c[ivalue+1]*(time[ivalue+3]-time[ivalue+2]))*args['discharge']) for ivalue,value in enumerate(cummMass)]
        except IndexError:
            print 'Error: time values are {}'.format(time.shape[0]),' they should be {}'.format(c.shape[0]), ' i.e an equal number of concentration values are required'
            sys.exit(1)
        cummMass.insert(0,0)   
        cummMass.insert(len(cummMass)+1,cummMass[len(cummMass)-1])
        cummMass.insert(len(cummMass)+2,cummMass[len(cummMass)-1]) 
        cummMass =np.array(cummMass)
        cummMass = np.cumsum(cummMass)
        cummMass_ = [(value/args['mass']) for ivalue, value in enumerate(cummMass)] 
        cummMass_ = np.array(cummMass_)
        
        #% mass recovery
        recovery = np.max(cummMass)/args['mass']
        
        # Percent mass
        percentMass = [(value/recovery) for ivalue, value in enumerate(cummMass_)] 
        percentMass = np.array(percentMass)
        
        # c/max
        cMax = [(value/np.max(c)) for ivalue, value in enumerate(c)] 
        cMax = np.array(cMax)
       
        #Get dominant time
        cMaxIdx = np.where(c==np.max(c))[0]
        cMaxIdx = np.min(cMaxIdx)    #in case we have maximum mass at two or more time steps
        tDominant = time[cMaxIdx]
       
        #Time of 16%, 50% and 84% mass recovery
        tRecovery = [0.16, 0.5, 0.84]
        print 'Time of {}'.format([x*100 for x in tRecovery]), ' % mass recovery is considered \n'
        pos = [((np.abs(percentMass-value)).argmin()) for tvalue, value in enumerate(tRecovery)]
        mSixteen = percentMass[pos[0]]
        tSixteen = time[pos[0]]
        mFifty = percentMass[pos[1]]
        tFifty = time[pos[1]]
        mEight = percentMass[pos[2]]
        tEight = time[pos[2]]
        
         #Get dominant, median and seepage velocity
        vDominant = args['length']/tDominant
        vMedian = (args['length']/tFifty)*24.0*3600
        seepageVel = (vDominant + (vMedian/(24*3600)))/2
        
        #Median time
        tMedian = (tFifty+tSixteen)/2
        
        #Longitudnal Dispersion coefficient and dispersivity
        longDispCoef = (((vMedian/(24*3600))**2)*(tEight-tSixteen)**2)/(8*tFifty)
        dispersivity = longDispCoef/seepageVel
        
        #Variance and peclet number
        variance = np.sqrt(2*dispersivity*args['length'])
        pecletNum = seepageVel*args['length']/longDispCoef
       
        #Write output to a csv file
        result_ = [tDominant,tSixteen,tFifty,tEight,vDominant,vMedian,seepageVel,tMedian,
                   longDispCoef,dispersivity,variance,pecletNum]
                   
        parameters_ = ['Dominant Time', 'T16', 'T50', 'T80','Dominant Velocity', 'Median Velocity',
                       'Seepage Velocity', 'Median Time', 'Longitudnal Dispersion Coefficient',
                       'Dispersivity', 'Variance', 'Peclet Number']
                       
        [(result_.insert(ivalue,'')) for ivalue in range(len(result_),PV.shape[0])]
        [(parameters_.insert(ivalue,'')) for ivalue in range(len(parameters_),PV.shape[0])]
        
        list_ = pd.DataFrame({'PV':PV, 'time [sec]':time,'c(cl) [mg/l]':c,
                'cumm.mass':cummMass,'percent mass':percentMass, 'tprameters_':parameters_, 'values':result_ })
        
        csvFile = outDirectory + basename + '.csv'   
        list_.to_csv(csvFile,  sheet_name='sheet2', index=False)
        
               
        # print the parameters 
        if args['verbosity'] == 2:
            with open(outDirectory + basename + '.csv') as file:
                print outDirectory + basename + '.csv'
                reader = csv.reader(file)
                paramList = list(reader)
                paramCounter=1
                for param in paramList[0]:
                    print "%-2i   %s" % (paramCounter, param)
                    paramCounter += 1
        
#        #Plotting the data
#        plt.ioff()
#        fig, ax1 = plt.subplots()
#        lns1 = ax1.plot(time,c, 'r--', label = 'c(cl) [mg/l]')
#        ax1.tick_params(axis='y', colors='red')
#        ax1.yaxis.label.set_color('red')
#        plt.yticks(yticks)
#        plt.xticks(xticks)
#        c_ = min(np.where(c==np.max(c))[0]) 
#        ax1.annotate('t = {},c = {}'.format(time[c_], round(c[c_],2)), xy=(time[c_], c[c_]), xycoords='data',
#                    xytext=(-15, 10), textcoords='offset points',
#                    arrowprops=dict(facecolor='black', shrink=0.001),
#                    horizontalalignment='right', verticalalignment='bottom',
#                    )
#        ax2 = ax1.twinx()
#        lns2 = ax2.plot(time,cummMass,'--', label = 'cmm. mass [mg]' )
#        ax1.set_xlabel ('Time [sec]',fontsize=20)
#        ax1.set_ylabel ('c(Cl) [mg/l]',fontsize=20)
#        ax2.set_ylabel ('cumm. mass[mg]',fontsize=20)
#        ax2.tick_params(axis='y', colors='blue')
#        ax2.yaxis.label.set_color('blue')
#        lns1, labels = ax1.get_legend_handles_labels()
#        lns2, labels2 = ax2.get_legend_handles_labels()
#        ax2.legend(lns1+lns2,labels + labels2, loc=0)
#        plt.tight_layout()
#        plt.grid(True)
#        plt.savefig('Tracer Test')
#        plt.show()
#        plt.close()
