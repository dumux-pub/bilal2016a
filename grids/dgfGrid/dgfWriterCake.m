%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write output file for radial cube grid
% structured grids
% path - path for dgf file
% dr - vector of radius coordinates
% da - vector of angles
% dz - vector of z coordinates
% wellRadius - Well radius at the tip of the cake slice

function dgfWriterCake(path, dr, da, dz, wellRadius)

if(checkUnique(dr) ~= 1 || checkUnique(da) ~= 1 || checkUnique(dz) ~=1)
    error('Input vectors are not unique');
end
if(checkAscending(dr) ~= 1 || checkAscending(da) ~= 1 || checkAscending(dz) ~=1)
    error('Input vectors are not ascending');
end

if(nargin == 5) %transformed structured case
    fid = fopen(path,'w');
    fprintf(fid, 'DGF\n');
    fprintf(fid, 'Vertex\n');
    z = 0;
    nodeNo(1)=0;
    for l=1:length(dz)
        for j=1:length(da)
            for i=1:length(dr)
                z = z+1;
                nodeNo(z) = z-1;
                dx = dr(i)*cos(da(j));
                dy = dr(i)*sin(da(j));
                fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz(l));
            end
        end
    end
    fprintf(fid, '#\n');
    
        fprintf(fid, 'CUBE\n');
    z = 0;
    for l=1:length(dz) -1
        for j=1:length(da) -1
            for i=1:length(dr) -1
                z = z+1;
                fprintf(fid, '%d %d %d %d %d %d %d %d\n', nodeNo(z), nodeNo(z+1), nodeNo(z+length(dr)), nodeNo(z+length(dr)+1), nodeNo(z+length(dr)*length(da)), nodeNo(z+length(dr)*length(da)+1), nodeNo(z+length(dr)*length(da)+length(dr)), nodeNo(z+length(dr)*length(da)+length(dr)+1));
            end
            z=z+1;
        end
        z=z+length(dr);
    end
    fprintf(fid, '#\n');
    fprintf(fid, 'BOUNDARYDOMAIN\n');
    fprintf(fid, 'default 1\n');
    fprintf(fid, '#\n');
    
else
    error('Wrong argument number for dgfWriterCake function: %s\n', nargin);
end

end