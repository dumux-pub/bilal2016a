%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vtk writer for 2 or 3d rectilinear grids
% pathToVTK - path for vtk file
% x - vector of x coordinates
% y - vector of y coordinates
% z - vector of z coordinates
% pathToMat - (optional) path to a mat file containing the coordinates 
% of all nodes, this is needed for deformed elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function vtkWriterNormal(pathToVTK, x, y, z, pathToMat)

if(checkUnique(x) ~= 1 || checkUnique(y) ~= 1)
    error('Input vectors are not unique');
end
if(checkAscending(x) ~= 1 || checkAscending(y) ~= 1)
    error('Input vectors are not ascending');
end

if (length(z) > 1)
    if (checkUnique(z) ~= 1)
        error('Input vectors are not unique');
    end
    if (checkAscending(z) ~= 1)
        error('Input vectors are not ascending');
    end
end

if(nargin == 4 || nargin == 5) %check whether number of input parameters
    %is correct
    fid = fopen(pathToVTK,'w');
    fprintf(fid, '# vtk DataFile Version 2.0\n');
    fprintf(fid, 'Rectilinear Grid\n');
    fprintf(fid, 'ASCII\n');
    fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
    fprintf(fid, 'POINTS %d FLOAT\n', length(x)*length(y)*length(z));
    counter = 1;
    nodeNo(1)=0;
    if(nargin == 4) %write the nodes for the not deformed case
        if(length(z) > 1) %3d case
            for l=1:length(z)
                for j=1:length(y)
                    for i=1:length(x)
                        nodeNo(counter) = counter-1;
                        counter = counter+1;
                        dx = x(i);
                        dy = y(j);
                        dz = z(l);
                        fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz);
                    end
                end
            end
        else  %2d case
            for j=1:length(y)
                for i=1:length(x)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    dx = x(i);
                    dy = y(j);
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,0.0);
                end
            end
        end
        
    else %write the nodes for the deformed case
        load(pathToMat);
        if(exist('coor','var'))
            if(length(z) > 1) %3d case
                for i=1:length(coor)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coor{i}(1), ...
                        coor{i}(2), ...
                        coor{i}(3));
                end
            else %2d case
                for i=1:length(coor)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    fprintf(fid, '%12.15f %12.15f \n ', ...
                        coor{i}(1), ...
                        coor{i}(2)), ...
                        0.0;
                end
            end
        else
            error('The mat file does not contain the variable "coor". ');
        end
    end
    if(length(z) > 1) %3d case
        numCells = (length(x)-1)*(length(y)-1)*(length(z)-1);
        numNodePerCell = 8;
    else %2d case
        numCells = (length(x)-1)*(length(y)-1);
        numNodePerCell = 4;
    end

    
    fprintf(fid, 'CELLS %d %d\n', numCells, numCells*(numNodePerCell+1));
    
    counter = 0;
    if(length(z) > 1) %3d case
        for l=1:length(z) -1
            for j=1:length(y) -1
                for i=1:length(x) -1
                    counter = counter+1;
                    fprintf(fid, '%d %d %d %d %d %d %d %d %d\n', ...
                        numNodePerCell, ...
                        nodeNo(counter), ...
                        nodeNo(counter+1), ...
                        nodeNo(counter+length(x)), ...
                        nodeNo(counter+length(x)+1), ...
                        nodeNo(counter+length(x)*length(y)), ...
                        nodeNo(counter+length(x)*length(y)+1), ...
                        nodeNo(counter+length(x)*length(y)+length(x)), ...
                        nodeNo(counter+length(x)*length(y)+length(x)+1));
                end
                counter=counter+1;
            end
            counter=counter+length(x);
        end
    else %2d case
        counter = 0;
        for j=1:length(y) -1
            for i=1:length(x)-1
                counter = counter+1;
                fprintf(fid,  '%d %d %d %d %d\n', ...
                    numNodePerCell, ...
                    nodeNo(counter), ...
                    nodeNo(counter+1), ...
                    nodeNo(counter+length(x)), ...
                    nodeNo(counter+length(x)+1));
            end
            counter=counter+1;
        end
    end
    
    fprintf(fid, 'CELL_TYPES %d \n', numCells);
    
 
   if(length(z) > 1) %3d case
        cellType = 11; %VTK_VOXEL
   else %2d case
       cellType = 8; %VTK_PIXEL
   end
  if(length(z) > 1) %3d case
      for l=1:length(z) -1
          for j=1:length(y) -1
              for i=1:length(x) -1
                  fprintf(fid, '%d\n', cellType);
              end
          end
      end
  else %2d case
      for j=1:length(y) -1
          for i=1:length(x) -1
              fprintf(fid, '%d\n', cellType);
          end
      end
  end
else
    error('Wrong argument number for dgfWriterNormal function: %s\n', nargin);
end
end