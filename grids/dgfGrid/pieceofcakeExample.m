% Cake cake grid
clear;

pathdgf = 'pieceofcake.dgf';
pathvtk = 'pieceofcake.vtk';
rMax = 0.5; % [m] maximum radius
A = 2*pi/2; % Domain angle
Z = 1; % Domain length in z direction
drMin = 0.01;
nSlice = 1; %no of slices
nZ = 85; %approximate no of elements in Z direction
cellSizeZ = Z/nZ;
wellRadius = 0.01; %Well radius at the tip of the cake slice
growFactor = 2; % Factor for increasing the distance between nodes away from the well

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dr Vector
% fill the vector dr the distance between nodes by growFactor
dr = 0:drMin:rMax;

% while dr(i) < rMax
%     i = i+1;
%     dr(i) = dr(i-1) + (dr(i-1) - dr(i-2))*growFactor;
% end

% if(dr(i) > rMax)
%     dr(i) = rMax;
% end

%da Vector
da = 0:A/nSlice:A;

%Z-Vector
dz = 0:Z/nZ:Z;

%display the number of nodes
noNodes = length(dr)*length(da)*length(dz);
display(noNodes);

%Create the dgf file
dgfWriterCake(pathdgf, dr, da, dz, wellRadius);

%Create the vtk file
vtkWriterCake(pathvtk, dr, da, dz, wellRadius);



