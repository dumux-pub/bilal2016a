%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write dgf output files for 2d and 3d for rectilinear grids
% structured grids
% pathToDGF - path for dgf file
% x - vector of radius coordinates
% y - vector of angles
% z - vector of z coordinates
% pathToMat - (optional) path to a mat file containing the coordinates 
% of all nodes, this is needed for deformed elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dgfWriterNormal(pathToDGF, x, y, z, pathToMat)

if(checkUnique(x) ~= 1 || checkUnique(y) ~= 1)
    error('Input vectors are not unique');
end

if(checkAscending(x) ~= 1 || checkAscending(y) ~= 1)
    error('Input vectors are not ascending');
end

if (length(z) > 1)
    if (checkUnique(z) ~= 1)
        error('Input vectors are not unique');
    end
    if (checkAscending(z) ~= 1)
        error('Input vectors are not ascending');
    end
end


if(nargin == 4 || nargin == 5) %check whether number of input parameters
    %is correct
    fid = fopen(pathToDGF,'w');
    fprintf(fid, 'DGF\n');
    fprintf(fid, 'Vertex\n');
    
    %write the nodes for the not deformed case
    if(nargin == 4)
        counter = 1;
        nodeNo(1)=0;
        if(length(z) > 1) %3d case
            for l=1:length(z)
                for j=1:length(y)
                    for i=1:length(x)
                        nodeNo(counter) = counter-1;
                        counter = counter+1;
                        dx = x(i);
                        dy = y(j);
                        dz = z(l);
                        fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz);
                    end
                end
            end
        else %2d case
            for j=1:length(y)
                for i=1:length(x)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    dx = x(i);
                    dy = y(j);
                    fprintf(fid, '%12.15f %12.15f \n ', dx,dy);
                end
            end
        end
        
    end
    
    %write the nodes for the deformed case
    if(nargin == 5)
        counter = 1;
        nodeNo(1)=0;
        load(pathToMat);
        if(exist('coor','var'))
            if(length(z) > 1)
                for i=1:length(coor)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coor{i}(1), ...
                        coor{i}(2), ...
                        coor{i}(3));
                end
            else
                for i=1:length(coor)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    fprintf(fid, '%12.15f %12.15f \n ', ...
                        coor{i}(1), ...
                        coor{i}(2));
                end
            end
        else
            error('The mat file does not contain the variable "coor". ');
        end
    end
    fprintf(fid, '#\n');
    
    
    % Write which nodes belong to which element
    fprintf(fid, 'CUBE\n');
    if(length(z) > 1) % 3d case
        counter = 0;
        for l=1:length(z) -1
            for j=1:length(y) -1
                for i=1:length(x) -1
                    counter = counter+1;
                    fprintf(fid, '%d %d %d %d %d %d %d %d\n', ...
                        nodeNo(counter), ...
                        nodeNo(counter+1), ...
                        nodeNo(counter+length(x)), ...
                        nodeNo(counter+length(x)+1), ...
                        nodeNo(counter+length(x)*length(y)), ...
                        nodeNo(counter+length(x)*length(y)+1), ...
                        nodeNo(counter+length(x)*length(y)+length(x)), ...
                        nodeNo(counter+length(x)*length(y)+length(x)+1));
                end
                counter=counter+1;
            end
            counter=counter+length(x);
        end
        
    else % 2d case
        counter = 0;
        for j=1:length(y) -1
            for i=1:length(x)-1
                counter = counter+1;
                fprintf(fid, '%d %d %d %d\n', ...
                    nodeNo(counter), ...
                    nodeNo(counter+1), ...
                    nodeNo(counter+length(x)), ...
                    nodeNo(counter+length(x)+1));
            end
            counter=counter+1;
        end
    end
    
    fprintf(fid, '#\n');
    fprintf(fid, 'BOUNDARYDOMAIN\n');
    fprintf(fid, 'default 1\n');
    fprintf(fid, '#\n');
    
else
    error('Wrong argument number for dgfWriterNormal function: %s\n', nargin);
end

end