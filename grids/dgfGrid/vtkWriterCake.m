%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%vtk writer for 2 or 3d rectilinear grids
% path - path for vtk file
% dx - vector of x coordinates
% dy - vector of y coordinates
% dz - vector of z coordinates
% wellRadius - Well radius at the tip of the cake slice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function vtkWriterCake(path, dr, da, dz, wellRadius)


fid = fopen(path,'w');
fprintf(fid, '# vtk DataFile Version 2.0\n');
fprintf(fid, 'Cake Grid\n');
fprintf(fid, 'ASCII\n');
fprintf(fid, 'DATASET UNSTRUCTURED_GRID\n');
fprintf(fid, 'POINTS %d FLOAT\n', length(dr)*length(da)*length(dz));
    z = 0;
    nodeNo(1)=0;
    for l=1:length(dz)
        for j=1:length(da)
            for i=1:length(dr)
                z = z+1;
                nodeNo(z) = z-1;
               
                dx = dr(i)*cos(da(j));
                dy = dr(i)*sin(da(j));
                
                %dx = cos(da(j))*wellRadius + cos(da(j))*dr(i);
                %dy = sin(da(j))*wellRadius + sin(da(j))*dr(i);
                fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz(l));
            end
        end
    end
    numCells = (length(dr)-1)*(length(da)-1)*(length(dz)-1);
    numNodePerCell = 8;
fprintf(fid, 'CELLS %d %d\n', numCells, numCells*(numNodePerCell+1));
    z = 0;
    for l=1:length(dz) -1
        for j=1:length(da) -1
            for i=1:length(dr) -1
                z = z+1;
                fprintf(fid, '%d %d %d %d %d %d %d %d %d\n', numNodePerCell, nodeNo(z), nodeNo(z+1), nodeNo(z+length(dr)), nodeNo(z+length(dr)+1), nodeNo(z+length(dr)*length(da)), nodeNo(z+length(dr)*length(da)+1), nodeNo(z+length(dr)*length(da)+length(dr)), nodeNo(z+length(dr)*length(da)+length(dr)+1));
            end
            z=z+1;
        end
        z=z+length(dr);
    end
fprintf(fid, 'CELL_TYPES %d \n', numCells);
cellType = 11; %VTK_VOXEL
    for l=1:length(dz) -1
        for j=1:length(da) -1
            for i=1:length(dr) -1
                fprintf(fid, '%d\n', cellType);
            end
        end
    end
end