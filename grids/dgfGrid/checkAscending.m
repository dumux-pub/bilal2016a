%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Check if a vector only has unique values returns 1 if this is the case
%otherwise 0
%vector = input vector to be checked

function [ascendingVar] = checkAscending(vector)


for i=1:length(vector)-1
    if(length(vector) > 1 && vector(i) > vector(i+1))
        ascendingVar = 0;
        break;
    else
        ascendingVar = 1;
    end
end
end