function [transformedCoorVector] = normal(coorVector)

if (length(coorVector) ~= 3) 
     error('Wrong dimension of input vector');
end

sigma = sqrt(0.25);
mu = 0;
scaleFactor = 1000;
transformedCoorVector(1) = coorVector(1);
transformedCoorVector(2) = coorVector(2);
radius = sqrt(coorVector(1)^2 + coorVector(2)^2);
transformedCoorVector(3) = coorVector(3) + scaleFactor*...
    1/(sigma*sqrt(2*pi))*exp(-(radius-mu)^2/(2*sigma^2));
end